/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_LOSS_LOSSLIST_H_
#define AIDGE_CORE_LOSS_LOSSLIST_H_

#include <cstddef>  // std::size_t
#include <memory>

#include "aidge/data/Tensor.hpp"

namespace Aidge {
namespace loss {

/**
 * @brief Compute the Mean Square Error loss.
 * This function returns the loss and set the ``grad()`` of the prediction
 * input.
 * @param prediction Tensor returned by the Aidge Graph, it is important that
 * this tensor is not a copy as overwise the backward function will not have a
 * gradient to start.
 * @param target Tensor representing the ground truth, it must be one hot encoded.
 */
Tensor MSE(std::shared_ptr<Tensor>& prediction,
           const std::shared_ptr<Tensor>& target);
Tensor BCE(std::shared_ptr<Tensor>& prediction,
           const std::shared_ptr<Tensor>& target);

}  // namespace loss
}  // namespace Aidge

#endif /* AIDGE_CORE_LOSS_LOSSLIST_H_ */
