/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPTIMIZER_OPTIMIZER_H_
#define AIDGE_CORE_OPTIMIZER_OPTIMIZER_H_

#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/learning/learningRate/LRScheduler.hpp"

namespace Aidge {

/**
 * @brief Interface for optimization classes.
 * Parameters to optimize and the learning rate scheduler should be specified outside
 * of the constructor in their own setter functions to avoid constructors with too
 * many parameters in derived classes.
 */
class Optimizer {
protected:
    /// @brief List of Tensors to update.
    std::vector<std::shared_ptr<Tensor>> mParameters{};
    /// @brief Learning rate scheduler.
    /// @note Initialized with constant learning rate.
    LRScheduler mLRScheduler = LRScheduler(1.0e-5f);

public:
    Optimizer() = default;

    virtual ~Optimizer() noexcept;

public:
    // getter & setters
    inline const std::vector<std::shared_ptr<Tensor>>& parameters() const noexcept {
        return mParameters;
    }

    virtual void setParameters(const std::vector<std::shared_ptr<Tensor>>& parameters) {
        mParameters = parameters;
        // for (const auto& param : parameters) {
        //     param->initGrad(); // create gradient and set it to zeros
        // }
    }

    constexpr float learningRate() const noexcept {
        return mLRScheduler.learningRate();
    }

    const LRScheduler& learningRateScheduler() const noexcept {
        return mLRScheduler;
    }

    void setLearningRateScheduler(const LRScheduler& lrscheduler) {
        mLRScheduler = lrscheduler;
    }

    /**
     * @brief Update each Tensor registered with respect to the associated uptade function.
     */
    virtual void update() {}

    /**
     * @brief Reset the gradient of each parameter registered in the Optimizer.
     */
    void resetGrad() const {
        for (const auto& t_ptr : mParameters) {
            t_ptr -> grad() -> zeros();
        }
    }
};

} // namespace Aidge

#endif // AIDGE_CORE_OPTIMIZER_OPTIMIZER_H_
