/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPTIMIZER_SGD_H_
#define AIDGE_CORE_OPTIMIZER_SGD_H_

#include <functional>
#include <memory>
#include <vector>

#include "aidge/data/Tensor.hpp"
#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/learning/optimizer/Optimizer.hpp"
#include "aidge/utils/StaticAttributes.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/TensorUtils.hpp"

namespace Aidge {

enum class SGDAttr {
    Momentum,
    Dampening
};

class SGD: public Optimizer, public StaticAttributes<SGDAttr, float, float> {
private:
    std::vector<Tensor> mGradientInertia;
    Tensor mLR{std::vector<std::size_t>({1})};
    Tensor mMomentum{std::vector<std::size_t>({1})};
    Tensor mReversedDampening{std::vector<std::size_t>({1})};

public:
    using Attributes_ = StaticAttributes<SGDAttr, float, float>;
    template <SGDAttr e>
    using attr = typename Attributes_::template attr<e>;

    SGD(const float momentum = 0.0f, const float dampening = 0.0f)
        : Optimizer(),
          Attributes_(attr<SGDAttr::Momentum>(momentum),
                    attr<SGDAttr::Dampening>(dampening))
    {
        mMomentum = Tensor(momentum);
        mReversedDampening = Tensor(1.0f - dampening);
    }

    void update() override final {
        mLR = Tensor(learningRate());
        mLR.setBackend(mParameters[0]->getImpl()->backend());

        if (mLRScheduler.step() == 0) {
            for (std::size_t i = 0; i < mParameters.size(); ++i) {
                mGradientInertia[i] = mParameters[i]->grad()->clone();
                *mParameters[i] -= mLR*mGradientInertia[i];
            }
        } else {
            for (std::size_t i = 0; i < mParameters.size(); ++i) {
                mGradientInertia[i] = mMomentum*mGradientInertia[i] + mReversedDampening*(*mParameters[i]->grad());
                *mParameters[i] -= mLR*mGradientInertia[i];
            }
        }
        mLRScheduler.update();
    }

    void setParameters(const std::vector<std::shared_ptr<Tensor>>& parameters) override final {
        Optimizer::setParameters(parameters);
        mGradientInertia = std::vector<Tensor>(parameters.size());
        for (std::size_t i = 0; i < parameters.size(); ++i) {
            mGradientInertia[i] = Tensor(parameters[i]->dims());
            mGradientInertia[i].setBackend(parameters[i]->backend());
        }
        if (parameters.size() > 0) {
            mReversedDampening.setBackend(mParameters[0]->getImpl()->backend());
            mMomentum.setBackend(mParameters[0]->getImpl()->backend());
        }
    }
};

} // namespace Aidge


namespace {
template <>
const char *const EnumStrings<Aidge::SGDAttr>::data[] = {
    "Momentum",
    "Dampening"
};
}
#endif // AIDGE_CORE_OPTIMIZER_SGD_H_
