/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CORE_OPTIMIZER_LRSCHEDULERSLIST_H_
#define AIDGE_CORE_OPTIMIZER_LRSCHEDULERSLIST_H_

#include "aidge/learning/learningRate/LRScheduler.hpp"

#include <cstddef>  // std::size_t

namespace Aidge {
namespace learning {

LRScheduler ConstantLR(const float initialLR);


LRScheduler StepLR(const float initialLR, const std::size_t stepSize, float gamma = 0.1f);

} // learning
} // namespace Aidge

#endif /* AIDGE_CORE_OPTIMIZER_LRSCHEDULERSLIST_H_ */
