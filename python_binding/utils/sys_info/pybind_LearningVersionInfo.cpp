
#include <pybind11/pybind11.h>
#include "aidge/utils/sys_info/LearningVersionInfo.hpp"

namespace py = pybind11;
namespace Aidge {
void init_LearningSysInfo(py::module& m){
    m.def("show_version", &showLearningVersion);
    m.def("get_project_version", &getLearningProjectVersion);
    m.def("get_git_hash", &getLearningGitHash);
}
}
