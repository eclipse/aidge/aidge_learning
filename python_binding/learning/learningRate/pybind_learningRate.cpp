/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/learning/learningRate/LRScheduler.hpp"
#include "aidge/learning/learningRate/LRSchedulerList.hpp"

namespace py = pybind11;
namespace Aidge {
// namespace learning {

void init_LRScheduler(py::module& m) {
    py::class_<LRScheduler, std::shared_ptr<LRScheduler>>(m, "LRScheduler")
    .def("step", &LRScheduler::step)
    .def("learning_rate", &LRScheduler::learningRate)
    .def("set_nb_warmup_steps", &LRScheduler::setNbWarmupSteps)
    .def("update", &LRScheduler::update)
    .def("lr_profiling", &LRScheduler::lr_profiling);

    m.def("constant_lr", &learning::ConstantLR, py::arg("initial_lr"));
    m.def("step_lr", &learning::StepLR, py::arg("initial_lr"), py::arg("step_size"), py::arg("gamma") = 0.1f);
}
// } // namespace learning
} // namespace Aidge
