/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/loss/LossList.hpp"

namespace py = pybind11;

namespace Aidge {

void init_Loss(py::module &m) {
    auto m_loss =
        m.def_submodule("loss", "Submodule dedicated to loss functions");
    m_loss.def("MSE", &loss::MSE, py::arg("graph"), py::arg("target"));
    m_loss.def("BCE", &loss::BCE, py::arg("graph"), py::arg("target"));
}
}  // namespace Aidge
