/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/learning/optimizer/Optimizer.hpp"
#include "aidge/learning/optimizer/Adam.hpp"

namespace py = pybind11;
namespace Aidge {
// namespace learning {

void init_Adam(py::module& m) {
    py::class_<Adam, std::shared_ptr<Adam>, Attributes, Optimizer>(m, "Adam", py::multiple_inheritance())
    .def(py::init<float, float, float>(), py::arg("beta1") = 0.9f, py::arg("beta2") = 0.999f, py::arg("epsilon") = 1.0e-8f)
    .def("update", &Adam::update);
}
// }  // namespace learning
}  // namespace Aidge
