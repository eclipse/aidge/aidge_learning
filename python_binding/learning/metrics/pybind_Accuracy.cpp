/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/learning/metrics/Accuracy.hpp"

namespace py = pybind11;

namespace Aidge {

void init_Accuracy(py::module &m) {
    auto m_metric = m.def_submodule("metrics", "Submodule dedicated to evaluation metrics");
    m_metric.def("Accuracy",&metrics::Accuracy, py::arg("prediction"), py::arg("target"), py::arg("axis"));
}
}  // namespace Aidge
