/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace Aidge {
// namespace learning {

void init_Loss(py::module&);
void init_Optimizer(py::module&);
void init_SGD(py::module&);
void init_Adam(py::module&);
void init_LRScheduler(py::module&);
void init_Accuracy(py::module&);
void init_LearningSysInfo(py::module&);

void init_Aidge(py::module& m) {
    init_Loss(m);
    init_Optimizer(m);
    init_SGD(m);
    init_Adam(m);
    init_Accuracy(m);
    init_LearningSysInfo(m);
    init_LRScheduler(m);
}

PYBIND11_MODULE(aidge_learning, m) { init_Aidge(m); }
// }  // namespace learning
}  // namespace Aidge
