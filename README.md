# aidge_learning

In this module, you can find functions and classes to train your models:

- ``Optimizer`` (SGD)
- ``LRScheduler`` (ConstantLR, StepLR)
- loss functions (MSE)

### Dependencies
- `GCC`
- `Make`/`Ninja`
- `CMake`
- `Python` (optional, if you have no intend to use this library in python with pybind)

#### Aidge dependencies
 - `aidge_core`
 - `aidge_backend_cpu`

## Pip installation
``` bash
pip install . -v
```
> **TIPS :** Use environment variables to change compilation options :
> - `AIDGE_INSTALL` : to set the installation folder. Defaults to /usr/local/lib. :warning: This path must be identical to aidge_core install path.
> - `AIDGE_PYTHON_BUILD_TYPE` : to set the compilation mode to **Debug** or **Release** 
> - `AIDGE_BUILD_GEN` : to set the build backend with 


## C++ installation

```bash
./setup.sh -m core -m backend_cpu -m learning --release
```
