/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/learning/learningRate/LRScheduler.hpp"

#include <cstddef>  // std::size_t
#include <vector>

std::vector<float> Aidge::LRScheduler::lr_profiling(const std::size_t nbStep) const {
    // instanciate the returned array
    std::vector<float> profile(nbStep);

    profile[0] = mInitialWarmUp; // equal to mInitialLR if no warm-up
    for (std::size_t step = 1; step < nbStep; ++step) {
        profile[step] = (step < mSwitchStep) ?
                            static_cast<float>(step + 1) * mInitialWarmUp :
                            mStepFunc(profile[step - 1], step);
    }
    return profile;
}
