/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/learning/learningRate/LRSchedulerList.hpp"

#include <cstddef>  // std::size_t

#include "aidge/learning/learningRate/LRScheduler.hpp"

Aidge::LRScheduler Aidge::learning::ConstantLR(const float initialLR) {
    return LRScheduler(initialLR);
}


Aidge::LRScheduler Aidge::learning::StepLR(const float initialLR, const std::size_t stepSize, float gamma) {
    return LRScheduler(initialLR,
                    [stepSize, gamma](float val, const std::size_t step) {
                        return (step % stepSize == 0) ? val*gamma : val;
                    });
}
