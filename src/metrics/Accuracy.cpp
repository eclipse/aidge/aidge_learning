/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <memory>
#include <numeric>  // std::iota

#include "aidge/data/Tensor.hpp"
#include "aidge/graph/GraphView.hpp"
#include "aidge/graph/OpArgs.hpp"
#include "aidge/learning/metrics/Accuracy.hpp"
#include "aidge/operator/OperatorTensor.hpp"
#include "aidge/operator/ArgMax.hpp"
#include "aidge/operator/ReduceSum.hpp"
#include "aidge/operator/And.hpp"
#include "aidge/recipes/GraphViewHelper.hpp"
#include "aidge/scheduler/Scheduler.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"

Aidge::Tensor Aidge::metrics::Accuracy(std::shared_ptr<Tensor>& prediction,
                                        const std::shared_ptr<Tensor>& target,
                                        std::int32_t axis) {
    /*
    Implementation note:
    Accuracy is computed using a graph in order to not be backend dependant.

    The graph used is the following:

    pred->ArgMax
                  ->And->ReduceSum
    label->ArgMax
    */

    AIDGE_ASSERT(target->dims().size() == 2,
                 "Label must have two dims: [BatchSize, NbChannel]");

    std::shared_ptr<Tensor> outputGrad = prediction->grad();

    AIDGE_ASSERT(prediction->backend() == target->backend(),
                 "'prediction' and 'target' Tensors must be on the "
                 "same backend. Found {} and {}.\n",
                 prediction->backend(), target->backend());
    AIDGE_ASSERT(prediction->dims() == target->dims(),
                 "'prediction' (shape {}) and 'target' (shape {}) Tensors must "
                 "have the same dimensions.\n",
                 prediction->dims(), target->dims());
    AIDGE_ASSERT(prediction->dataType() == target->dataType(),
                 "'prediction' (data type {}) and 'target' (data type {}) "
                 "Tensors must have the same data type.\n",
                 prediction->dataType(), target->dataType());

    // Create graph nodes and connections
    const std::shared_ptr<Node> argmax_perd_node = ArgMax(axis);
    const std::shared_ptr<Node> argmax_target_node = ArgMax(axis);

    const std::shared_ptr<Node> and_node = And();
    const std::shared_ptr<Node> rs_node = ReduceSum();

    const std::shared_ptr<Node> pred_node = Producer(prediction, "pred");
    pred_node->addChild(argmax_perd_node);
    const std::shared_ptr<Node> label_node = Producer(target, "label");
    label_node->addChild(argmax_target_node);

    argmax_perd_node->addChild(and_node,0,0);
    argmax_target_node->addChild(and_node,0,1);

    // and_node->addChild(rs_node,0,0);

    // Create the graph
    std::shared_ptr<GraphView> gv_local =
        Sequential({ and_node, rs_node});

    gv_local->add({pred_node,argmax_perd_node, label_node,argmax_target_node});
    gv_local->compile(prediction->getImpl()->backend(), prediction->dataType());

    SequentialScheduler ss_local{gv_local};
    ss_local.forward(false);

    // TODO: way too complicated to access
    const std::shared_ptr<OperatorTensor> res =
        std::dynamic_pointer_cast<OperatorTensor>(rs_node->getOperator());
    std::shared_ptr<Tensor> fallback;

    return res->getOutput(0)->refFrom(fallback, "cpu");
}
