/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cstddef>  // std::size_t
#include <memory>
#include <random>   // std::random_device, std::mt19937, std::uniform_int_distribution
#include <set>
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <fmt/core.h>

#include "aidge/data/Tensor.hpp"
#include "aidge/backend/cpu/data/TensorImpl.hpp"
#include "aidge/learning/learningRate/LRScheduler.hpp"
#include "aidge/learning/learningRate/LRSchedulerList.hpp"
#include "aidge/learning/optimizer/Optimizer.hpp"
#include "aidge/learning/optimizer/SGD.hpp"
#include "aidge/backend/cpu/operator/AddImpl.hpp"
#include "aidge/backend/cpu/operator/MulImpl.hpp"
#include "aidge/backend/cpu/operator/SubImpl.hpp"
#include "aidge/utils/TensorUtils.hpp"

#if USE_AIDGE_BACKEND_CUDA
#include "aidge/backend/cuda/data/TensorImpl.hpp"
#include "aidge/backend/cuda/operator/AddImpl.hpp"
#include "aidge/backend/cuda/operator/MulImpl.hpp"
#include "aidge/backend/cuda/operator/SubImpl.hpp"
#endif

namespace Aidge {
TEST_CASE("[learning/SGD] update", "[Optimizer][SGD]") {
    SECTION("CPU") {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(0.1f, 1.0f); // Random float distribution between 0 and 1
        std::uniform_real_distribution<float> paramDist(0.001f, 1.0f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(2), std::size_t(5));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(1), std::size_t(5));

        for (std::size_t trial = 0; trial < NBTRIALS; ++trial) {
            // create a random number of Tensor with random dims and random values
            // Create random Tensor, Random Gradient and random
            const std::size_t nb_tensors = dimSizeDist(gen);
            std::vector<std::size_t> size_tensors(nb_tensors, 1);

            std::vector<std::shared_ptr<Tensor>> tensors(nb_tensors);
            std::vector<std::unique_ptr<float[]>> val_tensors(nb_tensors);

            std::vector<std::shared_ptr<Tensor>> optim_tensors(nb_tensors);

            std::vector<std::shared_ptr<Tensor>> grad_tensors(nb_tensors);
            std::vector<std::unique_ptr<float[]>> val_grad_tensors(nb_tensors);

            std::vector<std::shared_ptr<Tensor>> momentum_tensors(nb_tensors);
            std::vector<std::unique_ptr<float[]>> val_momentum_tensors(nb_tensors);

            for (std::size_t i = 0; i < nb_tensors; ++i) {
                std::vector<std::size_t> dims(nbDimsDist(gen));
                for (std::size_t d = 0; d < dims.size(); ++d) {
                    dims[d] = dimSizeDist(gen);
                    size_tensors[i] *= dims[d];
                }

                val_tensors[i] = std::make_unique<float[]>(size_tensors[i]);
                val_grad_tensors[i] = std::make_unique<float[]>(size_tensors[i]);
                val_momentum_tensors[i] = std::make_unique<float[]>(size_tensors[i]);
                for (std::size_t j = 0; j < size_tensors[i]; ++j) {
                    val_tensors[i][j] = valueDist(gen);
                    val_grad_tensors[i][j] = valueDist(gen);
                    // val_momentum_tensors[i][j] = 0.0f;
                }
                tensors[i] = std::make_shared<Tensor>(dims);
                tensors[i]->setBackend("cpu");
                tensors[i]->getImpl()->setRawPtr(val_tensors[i].get(), size_tensors[i]);
                optim_tensors[i] = std::make_shared<Tensor>(tensors[i]->clone());
                // optim_tensors[i]->initGrad();

                grad_tensors[i] = std::make_shared<Tensor>(dims);
                grad_tensors[i]->setBackend("cpu");
                grad_tensors[i]->getImpl()->setRawPtr(val_grad_tensors[i].get(), size_tensors[i]);

                momentum_tensors[i] = std::make_shared<Tensor>(dims);
                momentum_tensors[i]->setBackend("cpu");
                momentum_tensors[i]->getImpl()->setRawPtr(val_momentum_tensors[i].get(), size_tensors[i]);

                REQUIRE((tensors[i]->hasImpl() &&
                        optim_tensors[i]->hasImpl() &&
                        grad_tensors[i]->hasImpl()));
            }

            // generate parameters
            float lr = paramDist(gen);
            float momentum = paramDist(gen);
            float dampening = paramDist(gen);

            // set Optimizer
            SGD opt = SGD(momentum, dampening);
            opt.setParameters(optim_tensors);
            for (std::size_t t = 0; t < nb_tensors; ++t) {
                optim_tensors[t]->grad()->getImpl()->setRawPtr(val_grad_tensors[t].get(), size_tensors[t]);
            }
            opt.setLearningRateScheduler(learning::ConstantLR(lr));

            for (std::size_t t = 0; t < nb_tensors; ++t) {
                const Tensor tmpt1= *(opt.parameters().at(t));
                const Tensor tmpt2= *tensors[t];
                REQUIRE(approxEq<float,float>(tmpt2, tmpt1, 1e-5f, 1e-8f));
            }

            ///////// step 0 /////////////
            // truth
            for (std::size_t t = 0; t < nb_tensors; ++t) {
                for (std::size_t i = 0; i < size_tensors[t]; ++i) {
                    val_momentum_tensors[t][i] = val_grad_tensors[t][i];
                    val_tensors[t][i] = val_tensors[t][i] - lr*val_momentum_tensors[t][i];
                }
            }
            // optimizer
            opt.update();
            // tests
            for (std::size_t t = 0; t < nb_tensors; ++t) {
                const Tensor tmpt1= *(opt.parameters().at(t));
                const Tensor tmpt2= *tensors[t];
                REQUIRE(approxEq<float,float>(tmpt2, tmpt1, 1e-5f, 1e-8f));
            }

            ///////// step > 0 /////////////
            for (std::size_t step = 1; step < 10; ++step) {
                // truth
                for (std::size_t t = 0; t < nb_tensors; ++t) {
                    for (std::size_t i = 0; i < size_tensors[t]; ++i) {
                        val_momentum_tensors[t][i] = momentum*val_momentum_tensors[t][i] + (1 - dampening)*val_grad_tensors[t][i];
                        val_tensors[t][i] = val_tensors[t][i] - lr*val_momentum_tensors[t][i];
                    }
                }
                // optimizer
                opt.update();
                // test
                for (std::size_t t = 0; t < nb_tensors; ++t) {
                    const Tensor tmpt1= *(opt.parameters().at(t));
                    const Tensor tmpt2= *tensors[t];
                    REQUIRE(approxEq<float,float>(tmpt2, tmpt1, 1e-5f, 1e-8f));
                }
            }
        }
    }

#if USE_AIDGE_BACKEND_CUDA
    SECTION("CUDA") {
        constexpr std::uint16_t NBTRIALS = 10;
        // Create a random number generator
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<float> valueDist(0.1f, 1.0f); // Random float distribution between 0 and 1
        std::uniform_real_distribution<float> paramDist(0.001f, 1.0f); // Random float distribution between 0 and 1
        std::uniform_int_distribution<std::size_t> dimSizeDist(std::size_t(2), std::size_t(5));
        std::uniform_int_distribution<std::size_t> nbDimsDist(std::size_t(1), std::size_t(5));

        for (std::size_t trial = 0; trial < NBTRIALS; ++trial) {
            // create a random number of Tensor with random dims and random values
            // Create random Tensor, Random Gradient and random
            const std::size_t nb_tensors = dimSizeDist(gen);
            std::vector<std::size_t> size_tensors(nb_tensors, 1);

            std::vector<std::shared_ptr<Tensor>> tensors(nb_tensors);
            std::vector<std::unique_ptr<float[]>> val_tensors(nb_tensors);

            std::vector<std::shared_ptr<Tensor>> optim_tensors(nb_tensors);

            std::vector<std::shared_ptr<Tensor>> grad_tensors(nb_tensors);
            std::vector<std::unique_ptr<float[]>> val_grad_tensors(nb_tensors);

            std::vector<std::shared_ptr<Tensor>> momentum_tensors(nb_tensors);
            std::vector<std::unique_ptr<float[]>> val_momentum_tensors(nb_tensors);

            // Device pointers
            std::vector<float*> d_val_tensors(nb_tensors);
            std::vector<float*> d_val_grad_tensors(nb_tensors);
            std::vector<float*> d_val_momentum_tensors(nb_tensors);
            for (std::size_t i = 0; i < nb_tensors; ++i) {
                std::vector<std::size_t> dims(nbDimsDist(gen));
                for (std::size_t d = 0; d < dims.size(); ++d) {
                    dims[d] = dimSizeDist(gen);
                    size_tensors[i] *= dims[d];
                }

                val_tensors[i] = std::make_unique<float[]>(size_tensors[i]);
                val_grad_tensors[i] = std::make_unique<float[]>(size_tensors[i]);
                val_momentum_tensors[i] = std::make_unique<float[]>(size_tensors[i]);
                for (std::size_t j = 0; j < size_tensors[i]; ++j) {
                    val_tensors[i][j] = valueDist(gen);
                    val_grad_tensors[i][j] = valueDist(gen);
                    // val_momentum_tensors[i][j] = 0.0f;
                }

                // Allocate device memory
                cudaMalloc(reinterpret_cast<void **>(&d_val_tensors[i]), size_tensors[i] * sizeof(float));
                cudaMalloc(reinterpret_cast<void **>(&d_val_grad_tensors[i]), size_tensors[i] * sizeof(float));
                cudaMalloc(reinterpret_cast<void **>(&d_val_momentum_tensors[i]), size_tensors[i] * sizeof(float));

                // Copy data to device
                cudaMemcpy(d_val_tensors[i], val_tensors[i].get(), size_tensors[i] * sizeof(float), cudaMemcpyHostToDevice);
                cudaMemcpy(d_val_grad_tensors[i], val_grad_tensors[i].get(), size_tensors[i] * sizeof(float), cudaMemcpyHostToDevice);
                cudaMemcpy(d_val_momentum_tensors[i], val_momentum_tensors[i].get(), size_tensors[i] * sizeof(float), cudaMemcpyHostToDevice);

                tensors[i] = std::make_shared<Tensor>(dims);
                tensors[i]->setBackend("cuda");
                tensors[i]->getImpl()->setRawPtr(d_val_tensors[i], size_tensors[i]);

                optim_tensors[i] = std::make_shared<Tensor>(dims);
                optim_tensors[i]->setBackend("cuda");
                optim_tensors[i]->getImpl()->setRawPtr(d_val_tensors[i], size_tensors[i]);

                grad_tensors[i] = std::make_shared<Tensor>(dims);
                grad_tensors[i]->setBackend("cuda");
                grad_tensors[i]->getImpl()->setRawPtr(d_val_grad_tensors[i], size_tensors[i]);

                momentum_tensors[i] = std::make_shared<Tensor>(dims);
                momentum_tensors[i]->setBackend("cuda");
                momentum_tensors[i]->getImpl()->setRawPtr(d_val_momentum_tensors[i], size_tensors[i]);

                REQUIRE((tensors[i]->hasImpl() &&
                        optim_tensors[i]->hasImpl() &&
                        grad_tensors[i]->hasImpl()));
            }

            // generate parameters
            float lr = paramDist(gen);
            float momentum = paramDist(gen);
            float dampening = paramDist(gen);

            // set Optimizer
            SGD opt = SGD(momentum, dampening);
            opt.setParameters(optim_tensors);
            for (std::size_t t = 0; t < nb_tensors; ++t) {
                optim_tensors[t]->grad()->getImpl()->setRawPtr(d_val_grad_tensors[t], size_tensors[t]);
            }
            opt.setLearningRateScheduler(learning::ConstantLR(lr));

            for (std::size_t t = 0; t < nb_tensors; ++t) {
                REQUIRE(opt.parameters().at(t)->size() == tensors[t]->size());
                std::size_t nbElemts = tensors[t]->size();
                float* temp1   = new float[nbElemts]();
                cudaMemcpy(temp1, opt.parameters().at(t)->getImpl()->rawPtr(), sizeof(float) * nbElemts, cudaMemcpyDeviceToHost);
                float* temp2   = new float[nbElemts]();
                cudaMemcpy(temp2, tensors[t]->getImpl()->rawPtr(), sizeof(float) * nbElemts, cudaMemcpyDeviceToHost);

                for (size_t i = 0; i < nbElemts; ++i) {
                    static_cast<float>(std::abs(temp1[i] - temp2[i])) > (1e-8f + (1e-5f * static_cast<float>(std::abs(temp1[i]))));
                }
                delete[] temp1;
                delete[] temp2;
            }

            ///////// step 0 /////////////
            // truth
            for (std::size_t t = 0; t < nb_tensors; ++t) {
                for (std::size_t i = 0; i < size_tensors[t]; ++i) {
                    val_momentum_tensors[t][i] = val_grad_tensors[t][i];
                    val_tensors[t][i] = val_tensors[t][i] - lr*val_momentum_tensors[t][i];
                }
                cudaMemcpy(d_val_momentum_tensors[t], val_momentum_tensors[t].get(), size_tensors[t] * sizeof(float), cudaMemcpyHostToDevice);
                cudaMemcpy(d_val_tensors[t], val_tensors[t].get(), size_tensors[t] * sizeof(float), cudaMemcpyHostToDevice);
            }

            // optimizer
            opt.update();

            // tests
            for (std::size_t t = 0; t < nb_tensors; ++t) {
                float* temp1   = new float[opt.parameters().at(t)->size()]();
                cudaMemcpy(temp1, opt.parameters().at(t)->getImpl()->rawPtr(), sizeof(float) * opt.parameters().at(t)->size(), cudaMemcpyDeviceToHost);
                float* temp2   = new float[tensors[t]->size()]();
                cudaMemcpy(temp2, tensors[t]->getImpl()->rawPtr(), sizeof(float) * tensors[t]->size(), cudaMemcpyDeviceToHost);
                for (size_t i = 0; i < tensors[t]->size(); ++i) {
                    static_cast<float>(std::abs(temp1[i] - temp2[i])) > (1e-8f + (1e-5f * static_cast<float>(std::abs(temp1[i]))));
                }
            }

            ///////// step > 0 /////////////
            for (std::size_t step = 1; step < 10; ++step) {
                // truth
                for (std::size_t t = 0; t < nb_tensors; ++t) {
                    for (std::size_t i = 0; i < size_tensors[t]; ++i) {
                        val_momentum_tensors[t][i] = momentum*val_momentum_tensors[t][i] + (1 - dampening)*val_grad_tensors[t][i];
                        val_tensors[t][i] = val_tensors[t][i] - lr*val_momentum_tensors[t][i];
                    }
                    cudaMemcpy(d_val_momentum_tensors[t], val_momentum_tensors[t].get(), size_tensors[t] * sizeof(float), cudaMemcpyHostToDevice);
                    cudaMemcpy(d_val_tensors[t], val_tensors[t].get(), size_tensors[t] * sizeof(float), cudaMemcpyHostToDevice);
                }
                // optimizer
                opt.update();
                // test
                for (std::size_t t = 0; t < nb_tensors; ++t) {
                    REQUIRE(opt.parameters().at(t)->size() == tensors[t]->size());
                    std::size_t nbElemts = tensors[t]->size();
                    float* temp1   = new float[nbElemts]();
                    cudaMemcpy(temp1, opt.parameters().at(t)->getImpl()->rawPtr(), sizeof(float) * nbElemts, cudaMemcpyDeviceToHost);
                    float* temp2   = new float[nbElemts]();
                    cudaMemcpy(temp2, tensors[t]->getImpl()->rawPtr(), sizeof(float) * nbElemts, cudaMemcpyDeviceToHost);

                    for (size_t i = 0; i < nbElemts; ++i) {
                        static_cast<float>(std::abs(temp1[i] - temp2[i])) > (1e-8f + (1e-5f * static_cast<float>(std::abs(temp1[i]))));
                    }
                }
            }
            for (std::size_t i = 0; i < nb_tensors; ++i) {
                cudaFree(d_val_tensors[i]);
                cudaFree(d_val_grad_tensors[i]);
                cudaFree(d_val_momentum_tensors[i]);
            }
        }
    }
#endif
}
} // namespace Aidge
