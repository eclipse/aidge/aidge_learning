"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import unittest
import aidge_core
import aidge_learning


class test_dummy(unittest.TestCase):
    """
    Dummy test to satisfy the CI
    """
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_dummy(self):
        self.assertEqual(1, 1)

if __name__ == '__main__':
    unittest.main()
