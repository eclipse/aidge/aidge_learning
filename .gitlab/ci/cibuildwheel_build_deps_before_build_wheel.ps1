$ErrorActionPreference = "Stop"

# Retrieve and clean the dependencies string from the environment variable
$AIDGE_DEPENDENCIES = $env:AIDGE_DEPENDENCIES -split ' '
Write-Host "Aidge dependencies : $AIDGE_DEPENDENCIES"
if ( $($AIDGE_DEPENDENCIES.Length) -eq 0) {
        Write-Host "- No dependencies provided for current repsitory"
        New-Item -ItemType Directory -Force -Path ".\build" | Out-Null
        Remove-Item -Path ".\build\*" -Recurse -Force
    } else {
        Write-Host "Retrieving given dependencies to build current package : $AIDGE_DEPENDENCIES"
    foreach ($dep in $($AIDGE_DEPENDENCIES -split " ")) {
        Write-Host "Retrieving : $dep"
        $curr_loc=$(Get-Location)
        Set-Location ../$dep
        Get-Location 
        Get-ChildItem .
        New-Item -Path ".\build" -ItemType Directory -Force | Out-Null
        Get-ChildItem -Path ".\build" -File | Remove-Item -Force
        python -m pip install . -v
        Set-Location $curr_loc
    }
}
